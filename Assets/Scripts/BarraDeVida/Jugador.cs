using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{

    public float velocidad;
    public Rigidbody2D rb;
    private float salto;
    private float direccionX;
    private Vector2 direccion;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        direccionX = Input.GetAxisRaw("Horizontal");
        salto = Input.GetAxisRaw("Vertical");
        direccion = new Vector2(direccionX, 0f).normalized;
    }

    private void FixedUpdate() {
        rb.velocity = new Vector2(direccion.x* velocidad, 0f);
            }
    
}
