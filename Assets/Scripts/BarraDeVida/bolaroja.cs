using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bolaroja : MonoBehaviour
{
    public float daño = 20.0f;

    private void OnTriggerEnter2D(Collider2D other) {
        
        if(other.gameObject.GetComponent<SistemaVida>() != null){
            other.gameObject.GetComponent<SistemaVida>().QuitarVida(daño);
        }

    }
}
