using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{
    public GameObject jugador;
    public Image barrVida;
    public float vidaA; 
    public float vidaM;



    // Update is called once per frame
    void Update()
    {
        vidaM = jugador.GetComponent<SistemaVida>().vidaMaxima;
        vidaA = jugador.GetComponent<SistemaVida>().vidaActual;

        barrVida.fillAmount = vidaA/vidaM;
    }
}
