using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaVerde : MonoBehaviour
{
    public float vida = 20.0f;

    private void OnTriggerEnter2D(Collider2D other) {
        
        if(other.gameObject.GetComponent<SistemaVida>() != null){
            other.gameObject.GetComponent<SistemaVida>().SumarVida(vida);
        }

    }
}
