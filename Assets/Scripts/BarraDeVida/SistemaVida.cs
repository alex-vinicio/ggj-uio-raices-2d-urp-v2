using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SistemaVida : MonoBehaviour
{

    public float vidaMaxima;
    public float vidaActual;
    public bool inmortal = false;
    public float tiempoImortal = 1.0f;
    public TMP_Text vidaText;

    // Start is called before the first frame update
    void Start()
    {
        vidaActual = vidaMaxima;
    }

    // Update is called once per frame
    void Update()
    {
     if (vidaActual >= vidaMaxima){
        vidaActual = vidaMaxima;
     }
     if (vidaActual <= 0){
        this.Muerte();
     }
     //UI
     vidaText.text = vidaActual.ToString();
    }
    // quitar vida
    public void QuitarVida(float daño){
        if(inmortal == true){
            return;
        }
        vidaActual -= daño;
        StartCoroutine(TiempoImortal());
    }
    public void SumarVida (float vida){
        vidaActual += vida;
    }
    
    public void Muerte(){
        Destroy(this.gameObject);
    }

    IEnumerator TiempoImortal(){
        inmortal =true;
        yield return new WaitForSeconds(tiempoImortal);
        inmortal = false;
    }
}
