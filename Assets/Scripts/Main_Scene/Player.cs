using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
	private int id;

	private string playerName;

	private int playerId;

	private string playerCode;

	private string operationDate;

	private string playerDetail;

	private float playerCcore;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public string getPlayerName()
	{
		return playerName;
	}

	public void setPlayerName(string playerName)
	{
		this.playerName = playerName;
	}

	public int getPlayerId()
	{
		return playerId;
	}

	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}

	public string getPlayerCode()
	{
		return playerCode;
	}

	public void setPlayerCode(string playerCode)
	{
		this.playerCode = playerCode;
	}

	public string getOperationDate()
	{
		return operationDate;
	}

	public void setOperationDate(string operationDate)
	{
		this.operationDate = operationDate;
	}

	public string getPlayerDetail()
	{
		return playerDetail;
	}

	public void setPlayerDetail(string playerDetail)
	{
		this.playerDetail = playerDetail;
	}

	public float getPlayerCcore()
	{
		return playerCcore;
	}

	public void setPlayerCcore(float playerCcore)
	{
		this.playerCcore = playerCcore;
	}

}
