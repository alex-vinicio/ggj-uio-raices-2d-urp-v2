using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour
{ 

    public void setNextScene(int indexScene)
    {
        SceneManager.LoadSceneAsync(indexScene);
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
