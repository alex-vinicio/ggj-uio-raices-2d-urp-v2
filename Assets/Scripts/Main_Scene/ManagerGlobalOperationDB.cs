using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ManagerGlobalOperationDB : MonoBehaviour
{
    public string httpUrlConect;
    public List<Player> players = new List<Player>();
    public Player player = new Player();

    void Start()
    {
        
    }

    IEnumerator getRequestAllPlayer(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                    break;
                case UnityWebRequest.Result.Success:
                    
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    
                    break;
            }
        }
    }

    public List<Player> getAllPlayers(string url)
    {
        // A correct website page.
        StartCoroutine(getRequestAllPlayer(url));
        return players;
    }

    public Player insertNewPlayer(string uri, string namePlayer, float score)
    {
        StartCoroutine(postInsertNewPlayer(uri, namePlayer, score, generateCodePlayer(namePlayer)));
        return player;
    }

    private string generateCodePlayer(string name)
    {
        System.DateTime today = System.DateTime.Now;
        string code = name + "-" + today.Day + "-" + today.Hour + today.Minute + "-" + WordFinder2(4) + today.Second;
        return code;
    }

    IEnumerator postInsertNewPlayer(string uri, string namePlayer,float score, string codePlayer)
    {
        WWWForm form = new WWWForm();
        string jsonFormet = namePlayer + ";" + score + ";" + codePlayer;
        form.AddField("player", jsonFormet);
        print(jsonFormet);
        UnityWebRequest www = UnityWebRequest.Post(uri, form);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Form upload complete!");
        }
    }

    public string WordFinder2(int requestedLength)
    {
        string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z" };
        string[] vowels = { "a", "e", "i", "o", "u" };

        string word = "";

        if (requestedLength == 1)
        {
            word = GetRandomLetter(vowels);
        }
        else
        {
            for (int i = 0; i < requestedLength; i += 2)
            {
                word += GetRandomLetter(consonants) + GetRandomLetter(vowels);
            }

            word = word.Replace("q", "qu").Substring(0, requestedLength); // We may generate a string longer than requested length, but it doesn't matter if cut off the excess.
        }

        return word;
    }

    private string GetRandomLetter(string[] letters)
    {
        return letters[Random.Range(0, letters.Length - 1)];
    }
}
