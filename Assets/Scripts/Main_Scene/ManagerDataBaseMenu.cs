using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ManagerDataBaseMenu : MonoBehaviour
{
    public ManagerGlobalOperationDB managerGlobalOperationDB;
    public TMP_Text namePlayer;
    public string basePath;
    public string url;

    void Start()
    {
        managerGlobalOperationDB = GetComponent<ManagerGlobalOperationDB>();
    } 


    public void setDataInDB()
    {
        string username = namePlayer.text;

        managerGlobalOperationDB.insertNewPlayer(basePath+url, username, 0f);
    }
}
